import React from 'react';

// "React.createContext" - is a function in React library that creates new context object.
// "Context Object" - is a data type of object that can be used to store information and can be shared to other components within the app.
const UserContext = React.createContext();

// "UserContext.Provider" - is a component that is created when using React.createContext().
// "Provider" component that allows other components to use the context object and/or supply necessary information needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;