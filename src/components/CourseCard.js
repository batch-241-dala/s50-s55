import { useState, useEffect } from 'react';
import {Button, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function CourseCard({course}) {
  
  const {name, description, price, _id} = course;

  // Use the state hook for this component to be able to store its state
  // States are used to keep track of information related to individual components
  /*
    SYNTAX:
    const [getter, setter] = useState(initialGetterValue);
  */
  // const [count, setCount] = useState(0);
  // const [seats, setSeats] = useState(30);
  // const [isOpen, setIsOpen] = useState(true);

  // function enroll() {
    
  //   setCount(count + 1);
  //   setSeats(seats - 1);

  // }

  // useEffect(() => {
  //     if (seats === 0) {
  //       setIsOpen(false);
  //       document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
  //     }
  //     // will run anytime one of the values in the array of the dependencies changes
  // }, [seats])

  return (
    <Card className = "p-3">
      <Card.Body>
        <h1>{name}</h1>
        <Card.Title>Description</Card.Title>
        <Card.Text>
          {description}
        </Card.Text>
        <Card.Title>Price</Card.Title>
        <Card.Text>
          Php {price}
        </Card.Text>
{/*        <Card.Text>
          Enrollees: {count}
        </Card.Text>
        <Card.Text>
          Seats: {seats}
        </Card.Text>*/}
        {/*<Button id={`btn-enroll-${id}`}className="bg-primary" onClick = {enroll}>View Details</Button>*/}
        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>View Details</Button>
      </Card.Body>
    </Card>
  );
}