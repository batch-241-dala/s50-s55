import { useState, useEffect} from 'react';

// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses(){

	// State that will be used to store the courses retrieved from database

	const [courses, setCourses] = useState([]);
	
	// Retrieves the courses from database upon initial render of the "Courses component"

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course => {
				return (
					<CourseCard key={course.id} course = {course} />
				)
			}))
		})
	}, [])
		

	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} course = {course}/>
	// 	)

	// })

	return (

		<>
		{courses}
		</>

	)

}