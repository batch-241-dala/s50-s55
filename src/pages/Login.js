import { useState, useEffect, useContext } from 'react';

import { Button, Form} from 'react-bootstrap';

import {Navigate} from 'react-router-dom'

import UserContext from '../UserContext'

import Swal from 'sweetalert2';

export default function Login() {

  // Allows us to use the UserContext object and its properties to be used for user validation
  const {user, setUser} = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

	// hook returns a function that lets you navigate to components

	// const navigate = useNavigate();

  function authenticate(e) {

  	e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {

      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({

        email: email,
        password: password

      })

    })
    .then(res => res.json())
    .then(data => {

      if(typeof data.access !== "undefined"){
        // The JWT will be used to retrieve user information accross the whole front end application and storing it in localStorage
        localStorage.setItem('token', data.access)
        retrieveUserDetails(data.access);

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to Zuitt!"
          })

      } else {

          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Please, check your login details and try again."
          })
      }


    })

  	// Set the email of the authenticated user in local storage
  	// localStorage.setItem(propertyName, value)
  	// localStorage.setItem('email', email);

    // Sets the global user state to have properties obtained from local storage
    // setUser({email: localStorage.getItem('email')});

    setEmail('');
    setPassword('');
  	// navigate('/')

  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {

      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }

    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

        // Global user state for validation accross the whole app
        // Changes the global "user" state to store the "id" and "isAdmin" property of the user which will be used for validation
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })

    })
  }


  useEffect(() => {

    if(email !== '' && password !== ''){
     setIsActive(true)
   } else {
     setIsActive(false)
   }

 }, [email, password])

  return (
    (user.id !== null) ?
    <Navigate to ="/courses"/>
    :
    <Form onSubmit={(e) => authenticate(e)}>
    <Form.Group className="mb-3" controlId="formBasicEmail">
    <h1>Login</h1>
    <Form.Label>Email address</Form.Label>
    <Form.Control 
    type="email" 
    placeholder="example@mail.com" 
    value = {email}
    onChange = {e => setEmail(e.target.value)}
    />
    <Form.Text className="text-muted">
    We'll never share your email with anyone else.
    </Form.Text>
    </Form.Group>

    <Form.Group className="mb-3" controlId="formBasicPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control 
    type="password" 
    placeholder="Password" 
    value = {password}
    onChange = {e => setPassword(e.target.value)}
    />
    </Form.Group>
    { isActive?
    <Button variant="success" type="submit">
    Submit
    </Button>
    :
    <Button variant="secondary" type="submit" disabled>
    Submit
    </Button>
  }
  </Form>
  );
}