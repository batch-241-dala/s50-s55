// import {Link} from 'react-router-dom'
// import {Button, Row, Col} from 'react-bootstrap';


// export default function NotFound() {
// return (
//     <Row>
//       <Col className="p-5">
//             <h1>Error 404</h1>
//             <p>Page not found.</p>
//             <Link to='/'><Button variant="primary">Go Back to homepage</Button></Link>
//         </Col>
//     </Row>
//   )
// }

import Banner from '../components/Banner';

export default function NotFound() {

	const data = {
		title: "Error 404 - Page Not Found",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to Home"

	}

	return (
		<Banner data={data}/>

	)

}
