import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';

import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom'

import Swal from 'sweetalert2';


export default function Register() {

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	// State to determine whether submit button will be enabled or not
	const [isActive, setIsActive] = useState(false);

	const { user } = useContext(UserContext);
	const navigate = useNavigate()

	function registerUser(e) {
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
			}),
		})
		.then((res) => res.json())
		.then((data) => {
			console.log(data);
			if (data) {
				Swal.fire({
					title: `Failed to Register`,
					icon: "error",
					text: `${email} is already registered`,
				});
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1,
					}),
				})
				.then((res) => res.json())
				.then((data) => {

					console.log(data);

					if (data) {
						Swal.fire({
							title: "Successfully Registered!",
							icon: "success",
							text: "Redirecting to Login Page"
						});
						setFirstName("");
						setLastName("");
						setEmail("");
						setMobileNo("");
						setPassword1("");
						setPassword2("");
						navigate("/login");
					} else {
						Swal.fire({
							title: "Failed to Register",
							icon: "error",
							text: "Please check your details"
						});
					}
				});
			}
		});
	}

	useEffect(() => {

		if((firstName !== '' && lastName !== '' && mobileNo !== '' && email !== '' && password1 !== '' && password2 !== '')&&(password1 === password2) &&(mobileNo.length == 11)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[firstName, lastName, mobileNo, email, password1, password2])

	return (
		(user.id !== null) ?
		<Navigate to ="/courses"/>
		:
		<Form onSubmit={(e) => registerUser(e)}>
		<Form.Group controlId="firstName">
		<Form.Label>First Name</Form.Label>
		<Form.Control 
		type="text"
		placeholder="First Name"
		value={firstName}
		onChange={e => setFirstName(e.target.value)} 
		required
		/>
		</Form.Group>
		<Form.Group controlId="lastName">
		<Form.Label>Last Name</Form.Label>
		<Form.Control 
		type="text"
		placeholder="Last Name"
		value={lastName}
		onChange={e => setLastName(e.target.value)} 
		required
		/>
		</Form.Group>
		<Form.Group controlId="userEmail">
		<Form.Label>Email address</Form.Label>
		<Form.Control 
		type="email" 
		placeholder="Enter email"
		value={email}
		onChange={e => setEmail(e.target.value)} 
		required
		/>
		<Form.Text className="text-muted">
		We'll never share your email with anyone else.
		</Form.Text>
		</Form.Group>

		<Form.Group controlId="password1">
		<Form.Label>Password</Form.Label>
		<Form.Control 
		type="password" 
		placeholder="Password"
		value={password1}
		onChange={e => setPassword1(e.target.value)} 
		required
		/>
		</Form.Group>

		<Form.Group controlId="password2">
		<Form.Label>Verify Password</Form.Label>
		<Form.Control 
		type="password" 
		placeholder="Verify Password" 
		value={password2}
		onChange={e => setPassword2(e.target.value)} 
		required
		/>
		</Form.Group>
		<Form.Group controlId="mobileNo">
		<Form.Label>Mobile Number</Form.Label>
		<Form.Control 
		type="number" 
		placeholder="+639 00 000 0000" 
		value={mobileNo}
		onChange={e => setMobileNo(e.target.value)} 
		required
		/>
		</Form.Group>
            {/*conditionally render submit button based on "isActive"*/}
		{ isActive ?
		<Button variant="primary" type="submit" id="submitBtn">
		Submit
		</Button>
		:
		<Button variant="secondary" type="submit" id="submitBtn">
		Submit
		</Button>
	}
	</Form>
	)

}